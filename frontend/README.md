KaraokeMaker Interface
=====================

There isn't yet any suitable graphic interface for this application.
But you can just use <code>curl</code> like the real chad you are.

## Random endpoint

You need to make requests to the **random** endpoint in order to get a random song from the specified artist.

```sh
 curl -X "GET" 'http://127.0.0.1:8000/play/random/daft%punk' | json_pp
```

Response should be somehow similar to a json like this :

```json
{
   "lyrics" : "Don't get any big ideas, They're not gonna happen",
   "suggested_youtube_url" : null,
   "artist" : "radiohead", 
   "title" : "Nude"
}
```

## Healthcheck endpoint

The healthcheck endpoint isn't yet functionnal. Do not trust it.



# Compile the client

But if you are feeling a little too good, and you've got a **.json** file ready, you can try to compile the C++ client.
I wish you a very good day.

## Various Dependencies

### Linux

First, you will need to install those things

```
sudo apt-get install -y build-essential
sudo apt install cmake
sudo apt-get install -y pkg-config
sudo apt install libcurlpp-dev
sudo apt-get install libcurl4-openssl-dev
``` 


### MacOS

Sadly, I don't have any MacOS machine available, but I'm pretty sure you can use **brew** to install all those stuff without any hiccups.

### Windows

If you are a proud Windows user, this is your lucky day ! You've got plenty of choices, and the freedom to follow your hearth on any of those ! In order to build the client, you can choose between those two differents options :

- the costly [one](https://www.apple.com/fr/mac/).
- the free [one](https://manjaro.org/).

Then just follow the instructions !

## Usage

Execute those commands in order to go to the ```build-dir``` directory and compile the client.


```sh
cd build-dir
cmake ..
make
```
If everything work (*spoiler* : it won't), all needed libraries should be installed and the program should compile.
You can then execute the client with this command :

```sh
./client --file path_to_your_file.json --lenght 4
```

The ```--lenght``` option allows you to decide how many songs you want. 

You can also directly give a valid json string without using the ```--file``` option :

```sh
./client '[{"artiste" : "Joy Division","note" : 18}, {"artiste": "Massive Attack", "note": 20}]' --lenght 2
```

The output is ugly, and sometimes crashes. It serves litterally no  purpose but to follow the instructions. But hey, you can enjoy karaoke making at native speed*, unlike all those simps and their *interpreted languages*.


<sub><sup>*doesn't take into account dev speed, nor debugging speed, nor compiling speed...</sup></sub>