#include "rapidjson/document.h"


#include <iostream>
#include <string>


class Manage_json
{
    public:
        rapidjson::Document load_json_file(const std::string filename);
        rapidjson::Document load_json(const std::string json_str);
        void print_json(const rapidjson::Document& doc);
};
