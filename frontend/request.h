#include <string>


class Request
{
    public:
    std::string make(std::string artist_name);
    Request(const std::string associated_URL);
    const std::string URL;
};
