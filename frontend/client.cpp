#include <stdio.h>
#include <map>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <string.h>
  
#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

#include "manage_json.h"
#include "request.h"


std::string choose_artist(const std::map<const int, std::string>& table, int random_nb)
{
	/* this way, every artist has a probability of 
	being chosen proportional to its grade*/
	for (const auto &p : table)
	{
		if (random_nb < p.first)
			return p.second;
	}
	return "";
}

inline const char * const BoolToString(bool b)
{
  return b ? "" : "\n";
}

std::string prettify(const rapidjson::Document& song)
{
	std::string output = std::string(song["artist"].GetString()) + " - " 
	+ song["title"].GetString() + "\r\r" +
	song["lyrics"].GetString() + "\n\n";

	// why the f**k artist and song doesn't print !!??
	printf("%s - ", song["artist"].GetString());
	printf("%s\n", song["title"].GetString());
 	std::cout << "\n";
	std::cout << song["lyrics"].GetString() << "\n";
	return output;
}

int main(int argc, char* argv[])
{
	if (argc < 2)
	{
		std::cout << "ERROR : no file path as arguments\n";
		return 0;
	}

	int lenght = 7;
	int k = 0;
	std::string filename = argv[1];
	bool use_file = false;


	for (int i = 0; i < argc; ++i) 
	{	
        if (!strcmp(argv[i], "--lenght") && i + 1 < argc)
		{
			// ideally we also want to check if this is a number
			// and ignore it if it isn't
			lenght = atoi(argv[i+1]);
		}
		if (!strcmp(argv[i], "--file") && i + 1 < argc)
		{
			use_file = true;
			filename = argv[i+1];
		}
	}

	srand (time(NULL));
    Manage_json json;
	Request song_request("http://127.0.0.1:8000/play/random/");
    rapidjson::Document input_doc;

	std::map<const int, std::string> artist_table;	


	if (use_file)
	    input_doc = json.load_json_file(filename);
	else
		input_doc = json.load_json(filename);
	
    for(const auto& point : input_doc.GetArray()){
		const rapidjson::Value& artiste = point["artiste"];
		k += point["note"].GetInt();
		artist_table[k] = artiste.GetString();   
    }

	for (int i = 0; i < lenght; ++i)
	{
		// randomly choose an artist from the json file
		std::string artist = choose_artist(artist_table, rand() % k);

		// make the request and prettify the result
		std::string response = song_request.make(artist);
		
		bool b = ((response=="Internal Server Error") || (response == ""));
		if (b)			
			std::cout << "Contactez le SAV\n";
		else
		{
			rapidjson::Document song = json.load_json(response);
			//json.print_json(song);
			prettify(song);
			std::cout << BoolToString(i == lenght - 1);
		}
	}
    return 0;
}


