#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

#include <iostream>
#include <string>

#include <sstream>  // to store the contents from the file
#include <fstream>  // to open & read the file

#include "manage_json.h"

using namespace rapidjson;

class JsonParserException : public std::runtime_error
{
  public:
    ~JsonParserException() {}
    JsonParserException(const std::string msg)
      : std::runtime_error(msg)
    {}
};


rapidjson::Document Manage_json::load_json_file(const std::string filename)
{
    // 1. Parse a JSON string into DOM.
    std::ifstream jFile(filename);
    if(!jFile.is_open()) 
        throw JsonParserException("Unable to open file " + filename + ".");

    std::stringstream contents;
    std::string res;

    contents << jFile.rdbuf();

    rapidjson::Document doc;
    doc.Parse(contents.str().c_str());

    return doc;
}

rapidjson::Document Manage_json::load_json(const std::string json_str)
{
    rapidjson::Document doc;
    doc.Parse(json_str.c_str());
    return doc;
}


void Manage_json::print_json(const rapidjson::Document& doc)
{
    // 3. Stringify the DOM
    StringBuffer buffer;
    Writer<StringBuffer> writer(buffer);
    doc.Accept(writer);
    std::cout << buffer.GetString() << std::endl;
    return;
}