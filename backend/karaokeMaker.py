from fastapi import FastAPI, Request

from getArtist import Artist
from theAudioDB import AudioDB
from lyricsDB import LyricsDB


import uvicorn

app = FastAPI()


@app.get("/play/random/{artist_name}")
async def play(artist_name: str, request: Request):
    # if you're favorite band has a % in its name...
    # well you can go to hell
    artist_name = artist_name.replace('%', ' ')
    artist = Artist(artist_name)
    access_lyrics = LyricsDB()

    song = await artist.get_random_song()
    # check for errors here
    song = song["strTrack"]
    lyrics = access_lyrics.get_lyrics(artist_name, song)
    first_lyrics = '\r'.join(lyrics.split('\r')[:2])
    first_lyrics = '\n'.join(first_lyrics.split('\n')[:2])
    return {
        "artist": artist.name,
        "title": song,
        "suggested_youtube_url": None,
        "lyrics": first_lyrics
    }

@app.get("/")
async def root():
    AudioDB_healthcheck = await AudioDB.healthcheck(AudioDB())
    LyricsDB_healthcheck = await LyricsDB.healthcheck(LyricsDB())
    return {"AudioDB_healthcheck": AudioDB_healthcheck,
            "LyricsDB_healthcheck": LyricsDB_healthcheck}


if __name__ == "__main__":
    uvicorn.run(app)
