from theAudioDB import AudioDB
import random

# import asyncio


class Artist:
    """All useful functions in order to retrieve data from
    a particular artist are grouped in this class.

    Note that if you're a patreon user of AudioDB and you can make
    a lot of API calls, you might prefere the asynchronous function
    get_random_songs, that can allow you to return multiples random songs
    from one artist without risks of duplicate.
    You will need to code only a few lines in the karaokeMaker file.
    """

    def __init__(self, artist_name) -> None:
        self.name = artist_name.lower()
        self.id = None

    async def get_discography(self)-> list:
        """returns all albums by the artist

        Returns:
            list: A list of valid json dictionaries
        """
        audioDB = AudioDB()
        albums = audioDB.get_artist_discography(self.name)
        return albums

    async def get_random_album(self):
        albums = self.get_discography()
        if albums["album"] is not None:
            return random.choice(albums["album"])["strAlbum"]
        else:
            return None

    # depreciated
    """async def get_random_song(self):
        audioDB = AudioDB()
        albums_id = await self.get_albums_id()
        # for weirds reasons it only works by directly invoquing make requests
        Tasks = [audioDB.make_request('track.php',
                                      {'m': id})
                 for id in albums_id]
        res = await asyncio.gather(*Tasks)
        songs = [song["track"] for song in res]
        return random.choice(songs) """

    async def get_random_song(self):
        audioDB = AudioDB()
        id = random.choice(await self.get_albums_id())
        songs = await audioDB.make_request('track.php', {'m': id})
        song = random.choice(songs["track"])
        return song

    async def get_albums_id(self):
        audioDB = AudioDB()
        self.id = await audioDB.get_artist_id(self.name)
        if self.id is not None:
            return await audioDB.get_albums_id(self.id)
        return None
