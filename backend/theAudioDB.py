import requests


class AudioDB:
    """ This class is here to represent accesss to a particular API
        All methods are variations around the make_request method, which
        does all the API calls.
    """
    def __init__(self) -> None:
        self.URL = 'https://theaudiodb.com/api/v1/json/2/'
        pass

    async def make_request(self, endpoint, params):
        response = requests.get(self.URL + endpoint, params=params)
        return response.json()

    async def get_artist_discography(self, artist_name):
        params = {
            's': artist_name,
        }
        return self.make_request('discography.php', params)

    async def get_artist_id(self, artist_name) -> int:
        params = {
            's': artist_name,
        }
        response = await self.make_request('search.php', params)
        return int(response["artists"][0]["idArtist"])

    async def get_albums_id(self, artist_id):
        params = {
            'i': artist_id,
        }
        response = await self.make_request('album.php', params)
        return [k["idAlbum"] for k in response["album"]]

    async def get_tracklist(self, album_id):
        params = {'m': album_id}
        return self.make_request('track.php', params)


    async def healthcheck(self):
        """this one is a bit tricky
        because it doesn't work as intended
        Would be better if audioDB had a proppoer healthcheck endpoint

        Returns:
            str I guess: the HTML status code of the response
        """
        res = requests.get(self.URL)
        return res.status_code
