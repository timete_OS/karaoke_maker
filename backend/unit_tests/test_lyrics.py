import unittest

from lyricsDB import LyricsDB


class Test_lyrics(unittest.TestCase):

    def test_lyrics_fine(self):
        access_lyrics = LyricsDB(mock=True)

        lyrics = access_lyrics.get_lyrics("artist_name", "song")
        first_lyrics = '\r'.join(lyrics.split('\r')[:2])
        self.assertEqual(first_lyrics, "Here the lyrics of the song")

    def test_lyrics_edge_case(self):
        access_lyrics = LyricsDB(mock=True)

        lyrics = access_lyrics.get_lyrics("", "")
        self.assertEqual(lyrics, '')
