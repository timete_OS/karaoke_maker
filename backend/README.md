KaraokeMaker Backend
=====================

## Modularity please

Every database used is in its own class. This way, it is easy to add or modify access to ressources.

## Quick summary of what's going on

Ideally we would want a cache with all discography of previously queried artists, in order to reduce the amount of requests peer requet.

```mermaid
graph LR;
    get_artist_id --> get_albums_id;
    get_albums_id --> get_random_song --> get_lyrics;



    get_lyrics-- save in cache --->id1[(Database)];

    get_lyrics -->return_to_client
```

## What's missing

There's **a lot** of edge case detection and error handling to make. For example, the way capital letters and special characters are handled in bands name (they are not handled) would deserve an update.