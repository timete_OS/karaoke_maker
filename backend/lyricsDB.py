import requests


class LyricsDB:

    def __init__(self, mock=False) -> None:
        self.URL = 'https://api.lyrics.ovh/v1/'
        if mock:
            self.URL = 'https://private-anon-4e6c93d946-lyricsovh.apiary-mock.com/v1/'

    def get_lyrics(self, artist, song) -> str:
        if min(len(artist), len(song)) == 0:
            return ''
        # need to capitalize artist name ?
        # unless there is aleready maj in it
        artist = '%20'.join([word.capitalize()
                            for word in artist.split()]) + '/'
        song = '%20'.join([word.capitalize() for word in song.split()])
        response = requests.get(self.URL + artist + song)
        if response.status_code != 200:
            return ""
        response_json = response.json()
        if "lyrics" in response_json.keys():
            return response_json["lyrics"]
        return ''

    async def healthcheck(self):
        res = requests.get(self.URL)
        return res.status_code
