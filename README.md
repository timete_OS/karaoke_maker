KaraokeMaker
=====================

## What is KaraokeMaker ?

A great way to quickly make your evenings better.

## Why is KaraokeMaker ?

Because we're all going to die.

## How it works ?

```mermaid
graph LR
    A[User] -->|json| B(Client)
    B -->|http requests| C{KaraokeMaker API}
    C -->|http requests| E(lyrics.ovh API)
    C -->|http requests| F(AudioDB API)
```

KaraokeMaker is just a kind of Meta-API. It will request others APIs in order to send random songs.

## Installation of the client

See the README in the client directory.

## Run the API

Install fastapi, and others requirements.

```sh
pip install -r requirements.txt
```

Install the ASGI server uvicorn.

```sh
pip install "uvicorn[standard]"
```

According to the FastAPI doc, it is very straitforward : navigate to the KaraokeMaker backend directory and execute :

```sh
Bruce@BatDesktop:~/KaraokeMaker/backend$ uvicorn KaraokeMaker:app
```

In the case of a windows machine you need to execute inside bash :

```bash
Robin@WindowsMachine:~/KaraokeMaker/backend$ python -m uvicorn KaraokeMaker:app
```

You can also just directly run the ```karaokeMaker.py``` file.

Bravo ! Now the API is up and running, and ready to accept requests !


## Testing

Running tests is as simple as executing a simple python file from the **backend** directory :

```sh
python tests.py
```

Note you can easily add new tests into the **backend/unit_tests** directory ! Feel free to do so.